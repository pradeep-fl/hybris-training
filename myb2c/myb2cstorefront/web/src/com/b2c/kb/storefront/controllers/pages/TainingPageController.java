package com.b2c.kb.storefront.controllers.pages;


import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
@RequestMapping("/traininfo")
public class TainingPageController extends AbstractPageController {

    @RequestMapping(method = RequestMethod.GET)
    public String home(final Model model) throws CMSItemNotFoundException
    {
        // used to set page details to model
        storeCmsPageInModel(model, getContentPageForLabelOrId("traininfo"));

        // used to set meta page details to model
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId("traininfo"));
        return getViewForPage(model);
    }
}
