/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.b2c.kb.initialdata.constants;

/**
 * Global class for all Myb2cInitialData constants.
 */
public final class Myb2cInitialDataConstants extends GeneratedMyb2cInitialDataConstants
{
	public static final String EXTENSIONNAME = "myb2cinitialdata";

	private Myb2cInitialDataConstants()
	{
		//empty
	}
}
