/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 11 Jan, 2020 9:01:48 PM                     ---
 * ----------------------------------------------------------------
 */
package com.b2c.kb.facades.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedMyb2cFacadesConstants
{
	public static final String EXTENSIONNAME = "myb2cfacades";
	
	protected GeneratedMyb2cFacadesConstants()
	{
		// private constructor
	}
	
	
}
