/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.b2c.kb.facades.constants;

/**
 * Global class for all Myb2cFacades constants.
 */
public class Myb2cFacadesConstants extends GeneratedMyb2cFacadesConstants
{
	public static final String EXTENSIONNAME = "myb2cfacades";

	private Myb2cFacadesConstants()
	{
		//empty
	}
}
