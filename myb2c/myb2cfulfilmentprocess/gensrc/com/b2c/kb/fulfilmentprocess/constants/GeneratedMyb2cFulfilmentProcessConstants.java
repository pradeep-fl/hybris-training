/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 11 Jan, 2020 9:01:48 PM                     ---
 * ----------------------------------------------------------------
 */
package com.b2c.kb.fulfilmentprocess.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedMyb2cFulfilmentProcessConstants
{
	public static final String EXTENSIONNAME = "myb2cfulfilmentprocess";
	public static class Attributes
	{
		public static class ConsignmentProcess
		{
			public static final String DONE = "done".intern();
			public static final String WAITINGFORCONSIGNMENT = "waitingForConsignment".intern();
			public static final String WAREHOUSECONSIGNMENTSTATE = "warehouseConsignmentState".intern();
		}
	}
	
	protected GeneratedMyb2cFulfilmentProcessConstants()
	{
		// private constructor
	}
	
	
}
